package com.hcl.app;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;

@RestController
public class StockController {
	private static Map<String, Stock> stockDataMap = new ConcurrentHashMap<>();

	static {
		stockDataMap.put("HCLTECH", new Stock("HCLTECH", "HCL Technologies Ltd.", new BigDecimal("38.55"),
				new BigDecimal("40.55"), System.currentTimeMillis()));
		stockDataMap.put("INFY", new Stock("INFY", "Infosys Ltd.", new BigDecimal("1.85"), new BigDecimal("2.85"),
				System.currentTimeMillis()));
		stockDataMap.put("TCS", new Stock("TCS", "Tata Consultancy Services Limited", new BigDecimal("1.05"),
				new BigDecimal("0.05"), System.currentTimeMillis()));
		stockDataMap.put("CTSH", new Stock("CTSH", "Cognizant Technology Solutions Corp.", new BigDecimal("180.97"),
				new BigDecimal("190.97"), System.currentTimeMillis()));
		stockDataMap.put("FB", new Stock("FB", "Facebook, Inc. Common Stock", new BigDecimal("2.48"),
				new BigDecimal("0.48"), System.currentTimeMillis()));
		stockDataMap.put("MOMO", new Stock("MOMO", "Momo Inc.", new BigDecimal("52.62"), new BigDecimal("50.62"),
				System.currentTimeMillis()));
		stockDataMap.put("TWTR", new Stock("TWTR", "Twitter Inc.", new BigDecimal("42.16"), new BigDecimal("40.16"),
				System.currentTimeMillis()));
		stockDataMap.put("RELIANCE", new Stock("RELIANCE", "Reliance Industries Limited", new BigDecimal("0.16"),
				new BigDecimal("2.16"), System.currentTimeMillis()));
	}

	@GetMapping(value = "/stocks", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<ServerSentEvent<Collection<Stock>>> liveStockChanges() {
		return Flux.interval(Duration.ofSeconds(5)).map(sec -> sec == 0 ? stockDataMap.values() : getRandomStock()).map(
				stocks -> ServerSentEvent.<Collection<Stock>>builder().event("stock-changed").data(stocks).build());
	}

	private List<Stock> getRandomStock() {
		Object symbolKey = stockDataMap.keySet().toArray()[new Random()
				.nextInt(stockDataMap.keySet().toArray().length)];
		LinkedList<Stock> randomStockData = new LinkedList<>();
		stockDataMap.values().stream().filter(stock -> stock.getStockSymbol().equalsIgnoreCase(symbolKey.toString()))
				.forEach(stock -> randomStockData.add(stock));

		for (Stock stock : randomStockData) {
			BigDecimal fixedBidPrice = new BigDecimal("2.5");
			BigDecimal fixedAskPrice = new BigDecimal("0.5");
			stock.setBidPrice(stock.getBidPrice().add(fixedBidPrice));
			stock.setAskPrice(stock.getAskPrice().add(fixedAskPrice));
			stock.setLastUpdated(System.currentTimeMillis());
		}
		return randomStockData;
	}
}
