package com.hcl.app;

import java.math.BigDecimal;

public class Stock {
	private String stockSymbol;
	private String companyName;
	private BigDecimal bidPrice;
	private BigDecimal askPrice;
	private long lastUpdated;

	public Stock() {
	}

	public Stock(String stockSymbol, String companyName, BigDecimal bidPrice, BigDecimal askPrice, long lastUpdated) {
		super();
		this.stockSymbol = stockSymbol;
		this.companyName = companyName;
		this.bidPrice = bidPrice;
		this.askPrice = askPrice;
		this.lastUpdated = lastUpdated;
	}

	public String getStockSymbol() {
		return stockSymbol;
	}

	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public BigDecimal getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(BigDecimal bidPrice) {
		this.bidPrice = bidPrice;
	}

	public BigDecimal getAskPrice() {
		return askPrice;
	}

	public void setAskPrice(BigDecimal askPrice) {
		this.askPrice = askPrice;
	}

	public long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public String toString() {
		return "Stock [stockSymbol=" + stockSymbol + ", companyName=" + companyName + ", bidPrice=" + bidPrice
				+ ", askPrice=" + askPrice + ", lastUpdated=" + lastUpdated + "]";
	}
}
