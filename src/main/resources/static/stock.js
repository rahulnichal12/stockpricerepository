function init() {
	$(document).ready(function() {
		setupStocksEventSrouce();
	});
}

function setupStocksEventSrouce() {
	var source = new EventSource('http://localhost:8080/stocks');
	source.addEventListener('stock-changed', function(e) {
		var updatedStocks = JSON.parse(e.data);
		updatedStocks.forEach(function(updatedStock) {
			updateStock(updatedStock);
		});

	}, false);
}

function updateStock(stock) {
	var stockDivContainer = $('.stocks-container');
	var stockDiv = $('#stock-' + stock.stockSymbol);
	var marketPriceValue = ((stock.bidPrice + stock.askPrice) / 2);
	var marketPrice = marketPriceValue.toFixed(2);
	var stockDivInnerHtml = '<span class="market-price"><span class="currency-symbol">$</span>'
			+ marketPrice
			+ '</span>'
			+ '<span class="glyphicon glyphicon-arrow-up arrow-up-show"></span>\n'
			+ '<h1>'
			+ stock.companyName
			+ '</h1>\n'
			+ '<h2>'
			+ stock.stockSymbol + '</h2>';

	if (stockDiv.length) {
		stockDiv.html(stockDivInnerHtml);
	} else {
		var stockDivHtml = '<div id="stock-' + stock.stockSymbol
				+ '" class="stock-widget">\n' + stockDivInnerHtml + '\n</div>';
		$('.stocks-container').append($(stockDivHtml));
	}
}

init();